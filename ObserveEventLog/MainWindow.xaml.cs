﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ObserveEventLog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static string source = "TestEventLog";
        private static string log = "TestLog";
        private EventLog sysEventLog = new EventLog(log);
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnCreateEventLog_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                txtResult.Text = "";
                if (!EventLog.SourceExists(source))
                    EventLog.CreateEventSource(source, log);
                sysEventLog.Source = source;
                sysEventLog.WriteEntry("Event log create success.", EventLogEntryType.SuccessAudit, 1);

                foreach (var el in EventLog.GetEventLogs("CHUCKIECOMP"))
                {
                    txtResult.Text += string.Format("{0} \n", el.Log);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Fail to create.\n{0}", ex.Message));
            }
        }

        private void BtnWriteEventLog_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string text = @txtText.Text;
                sysEventLog.WriteEntry(text, EventLogEntryType.Information, 2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Fail to write.\n{0}", ex.Message));
            }
        }

        private void BtnReadEventLog_Click(object sender, RoutedEventArgs e)
        {
            txtResult.Text = string.Format("Read on {0} {1}\n", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString());
            var ELEntryCollection = sysEventLog.Entries;
            foreach (EventLogEntry entry in ELEntryCollection)
            {
                txtResult.Text += string.Format("{0} {1}: ID: {2:000} -> {3}\n", entry.TimeGenerated.ToShortDateString(),
                    entry.TimeGenerated.ToLongTimeString(), entry.InstanceId, entry.Message);
            }
        }

        private void BtnClearEventLog_Click(object sender, RoutedEventArgs e)
        {
            sysEventLog.Clear();
            BtnReadEventLog_Click(sender, e);
        }
    }
}
